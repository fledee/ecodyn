EcoDynElec: Dynamic Life Cycle Assessment of electricity for ENTSO-E countries
==============================================================================

EcoDynElec software tracks the origin of electricity accross european countries based on generation and cross-border exchanges and allows the dynamic evaluation of environmental impacts of electricity.

.. image:: images/workflow.png

``ecodynelec`` is a free software under APACHE 2.0 licence. It was developped in a collaboration between the `EMPA <https://www.empa.ch/>`_, `HEIG-VD <https://heig-vd.ch/>`_, the `SUPSI <https://www.supsi.ch/home.html>`_.

Installation
============
For now the software can only be installed from the `gitlab repository <https://gitlab.com/fledee/ecodynelec/>`_

Contributions
=============
EcoDynElec did contribute to the project `EcoDynBat - Ecobilan Dynamique des Bâtiments <https://www.aramis.admin.ch/Texte/?ProjectID=41804>`_.

.. image:: images/logo.png

.. toctree::
    :maxdepth: 1
    :caption: Supplementary

    supplementary/download
    supplementary/mapping_usage
    supplementary/parameters
    supplementary/auxilary_files
    supplementary/functional_unit

.. toctree::
    :maxdepth: 3
    :caption: Structure

    structure/architecture
    structure/data_loading
    structure/load_impacts
    structure/tracking
    structure/impacts
    structure/local_residual

.. toctree::
    :maxdepth: 1
    :caption: Examples

    examples/Getting_started
    examples/Handle_parameters
    examples/Downloading
    examples/Execute_main_pipeline
    examples/Exploit_results
    examples/Electricity_mix_map

.. toctree::
    :maxdepth: 2
    :caption: Modules
    :hidden:

    modules/pipelines
    modules/parameter
    modules/impacts
    modules/tracking
    modules/preprocessing
    modules/saving
    modules/checking
    modules/updating




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
